//% color="#B34229" iconWidth=38 iconHeight=28
namespace Horse{

    //% block="千里马初始化" blockType="command"
    export function chollima_init(parameter: any, block: any) {
        Generator.addImport("from mpython import *");
        Generator.addImport("from chollima import *");
        Generator.addImport("import neopixel");
        Generator.addInitHeader(`chollima`, `chollima=CHOLLIMA()`);
        Generator.addInitHeader(`chollima_rgb`, `chollima_rgb = neopixel.NeoPixel(Pin(Pin.P7), n=10, bpp=3, timing=1)`);
    }

    //% block="千里马获取颜色" blockType="reporter"
    export function get_all_color(parameter: any,block:any){
        Generator.addCode(`chollima.getRGB()`);
    }

    //% block="千里马获取[RGB]" blockType="reporter"
    //% RGB.shadow="dropdown" RGB.options="RGB"
    export function get_color(parameter:any, block:any){
        let rgb = parameter.RGB.code;
        Generator.addCode(`chollima.getRGB()[${rgb}]`);
    }

    //% block="千里马获取HSV[HSV]" blockType="reporter"
    //% HSV.shadow="dropdown" HSV.options="HSV"
    export function get_hsv(parameter: any, block: any) {
        let hsv = parameter.HSV.code;
        Generator.addCode(`chollima.getHSV()[${hsv}]`);
    }

    //% block="千里马识别[COLOR]" blockType="boolean"
    //% COLOR.shadow="dropdown" COLOR.options="COLOR"
    export function discern_color(parameter: any, block: any) {
        let color = parameter.COLOR.code;
        Generator.addCode(`chollima.discern_${color}()`);
    }

    //% block="千里马超声波" blockType="reporter"
    export function ultrasonic(parameter: any, block: any) {
        Generator.addCode(`chollima.distance()`);
    }


    //% block="千里马循迹传感器设置阈值[THRESHOLD]" blockType="command"
    //% THRESHOLD.shadow="number" THRESHOLD.defl="100"
    export function set_line_threshold(parameter: any, block: any) {
        let threshold = parameter.THRESHOLD.code;
        Generator.addCode(`chollima.set_line_threshold(${threshold})`);
    }

    //% block="千里马循迹获取[LINE_N]循线值" blockType="reporter"
    //% LINE_N.shadow="dropdown" LINE_N.options="LINE_N"
    export function read_sensor(parameter: any, block: any) {
        let line_n = parameter.LINE_N.code;
        Generator.addCode(`chollima.read_sensor(${line_n})`);
    }

    //% block="千里马[LINE_N]号传感器碰到黑线" blockType="boolean"
    //% LINE_N.shadow="dropdown" LINE_N.options="LINE_N"
    export function touch_sensor(parameter: any, block: any) {
        let line_n = parameter.LINE_N.code;
        Generator.addCode(`chollima.touch_line(${line_n})`);
    }

    //% block="千里马[MOTION]速度[SPEED]" blockType="command"
    //% MOTION.shadow="dropdown" MOTION.options="MOTION"
    //% SPEED.shadow="range" SPEED.params.min=0 SPEED.params.max=100 SPEED.defl=100
    export function chollima_motion(parameter: any, block: any) {
        let motion = parameter.MOTION.code;
        let speed = parameter.SPEED.code;
        Generator.addCode(`chollima.${motion}(${speed})`);
    }

    //% block="千里马停止" blockType="command"
    export function chollima_stop(parameter: any, block: any) {
        Generator.addCode(`chollima.stop()`);
    }

    //% block="千里马左电机[STATUSL]速度[SPEEDL]右电机[STATUSR]速度[SPEEDR]" blockType="command"
    //% STATUSL.shadow="dropdown" STATUSL.options="STATUSL"
    //% STATUSR.shadow="dropdown" STATUSR.options="STATUSL"
    //% SPEEDL.shadow="range" SPEEDL.params.min=0 SPEEDL.params.max=100 SPEEDL.defl=100
    //% SPEEDR.shadow="range" SPEEDR.params.min=0 SPEEDR.params.max=100 SPEEDR.defl=100
    export function set_motor(parameter: any, block: any) {
        let statusl = parameter.STATUSL.code;
        let statusr = parameter.STATUSR.code;
        let speedl = parameter.SPEEDL.code;
        let speedr = parameter.SPEEDR.code;
        if (statusl == "0" && statusr == "0")
            Generator.addCode(`chollima.set_motor_speed(${speedl}, ${speedr})`);
        else if (statusl == "0" && statusr == "1")
            Generator.addCode(`chollima.set_motor_speed(${speedl}, -${speedr})`);
        else if (statusl == "1" && statusr == "0")
            Generator.addCode(`chollima.set_motor_speed(-${speedl}, ${speedr})`);
        else if (statusl == "1" && statusr == "1")
            Generator.addCode(`chollima.set_motor_speed(-${speedl}, -${speedr})`);
    }

        
    //% block="千里马获取电量值" blockType="reporter"
    export function read_battery_voltage(parameter: any, block: any) {
        Generator.addCode(`chollima.read_battery_voltage()`);
    }

    //% block="千里马获取左电机编码值" blockType="reporter"
    export function get_motor_l_encoder(parameter: any, block: any) {
        Generator.addCode(`chollima.get_motor_l_encoder()`);
    }

    //% block="千里马获取右电机编码值" blockType="reporter"
    export function get_motor_r_encoder(parameter: any, block: any) {
        Generator.addCode(`chollima.get_motor_r_encoder()`);
    }
    
    //% block="---"
    export function noteSep() {

    }

    //% block="千里马彩灯亮度[BRIGHT]" blockType="command"
    //% BRIGHT.shadow="range" BRIGHT.params.min=0 BRIGHT.params.max=100 BRIGHT.defl=100
    export function chollima_rgb_brightness(parameter: any, block: any) {
        let bright = parameter.BRIGHT.code;
        Generator.addCode(`chollima_rgb.brightness(${bright} / 100)`);
    }
    //% block="千里马[RGB_NUM]号灯亮[COLOR]" blockType="command"
    //% COLOR.shadow="colorPalette" 
    //% RGB_NUM.shadow="number" RGB_NUM.defl="0"
    export function chollima_rgb_num_on(parameter: any, block: any) {
        let rgb_num = parameter.RGB_NUM.code;
        let color = parameter.COLOR.code;
        color = color.replace("#", "0x");
        color = parseInt(color);
        var red = color >> 16 & 0xff;
        var green = color >> 8 & 0xff;
        var blue = color & 0xff;
        Generator.addCode(`chollima_rgb[${rgb_num}] = (${red},${green},${blue})`);
        Generator.addCode(`chollima_rgb.write()`);
    }

    //% block="千里马[RGB_NUM]号灯亮红[R]绿[G]蓝[B]" blockType="command"
    //% RGB_NUM.shadow="number" RGB_NUM.defl="0"
    //% R.shadow="number" R.defl="0"
    //% G.shadow="number" G.defl="0"
    //% B.shadow="number" B.defl="0"
    export function chollima_rgb_num_ona(parameter: any, block: any) {
        let rgb_num = parameter.RGB_NUM.code;
        let red = parameter.R.code;
        let green = parameter.G.code;
        let blue = parameter.B.code;
        Generator.addCode(`chollima_rgb[${rgb_num}] = (${red},${green},${blue})`);
        Generator.addCode(`chollima_rgb.write()`);
    }


    //% block="千里马所有灯全亮[COLOR]" blockType="command"
    //% COLOR.shadow="colorPalette" 
    export function chollima_rgb_all_on(parameter: any, block: any) {
        let color = parameter.COLOR.code;
        color = color.replace("#", "0x");
        color = parseInt(color);
        var red = color >> 16 & 0xff;
        var green = color >> 8 & 0xff;
        var blue = color & 0xff;
        Generator.addCode(`chollima_rgb.fill((${red},${green},${blue}))`);
        Generator.addCode(`chollima_rgb.write()`);
    }

    //% block="千里马所有灯全灭" blockType="command"
    export function chollima_rgb_all_off(parameter: any, block: any) {
        Generator.addCode(`chollima_rgb.fill((0,0,0))`);
        Generator.addCode(`chollima_rgb.write()`);
    }

    //% block="千里马[HEAD_NUM]号灯亮红[R]绿[G]蓝[B]" blockType="command"
    //% HEAD_NUM.shadow="dropdown" HEAD_NUM.options="HEAD_NUM"
    //% R.shadow="number" R.defl="0"
    //% G.shadow="number" G.defl="0"
    //% B.shadow="number" B.defl="0"
    export function chollima_rgb_all_ona(parameter: any, block: any) {
        let head_num = parameter.HEAD_NUM.code;
        let red = parameter.R.code;
        let green = parameter.G.code;
        let blue = parameter.B.code;
        Generator.addCode(`chollima.set_ws2812_rgb(${head_num},${red},${green},${blue})`);
    }

    //% block="---"
    export function noteSep1() {

    }
    //% block="切换到[MODE]模式" blockType="command"
    //% MODE.shadow="dropdown" MODE.options="MODE"
    export function Mode(parameter: any, block: any) {
        let mode = parameter.MODE.code;
        Generator.addCode(`chollima.mode_change(${mode})`);
    }

    //% block="从中心方框中学习" blockType="command"
    export function ark_learn(parameter: any, block: any) {
        Generator.addCode(`chollima.learn_from_center()`);
    }

    //% block="清除学习数据" blockType="command"
    export function clen_learn(parameter: any, block: any) {
        Generator.addCode("chollima.clean_data()");
    }

    //% block="方框是否存在数据" blockType="boolean"
    export function ai_get_data(parameter: any, block: any) {
        Generator.addCode(`chollima.get_ai_data()`);
    }

    //% block="获取当前ID" blockType="reporter""
    export function ai_get_class_id(parameter: any, block: any) {
        Generator.addCode(`chollima.get_class_id()`);

    }

    //% block="获取当前置信度" blockType="reporter""
    export function ai_get_class_value(parameter: any, block: any) {
        Generator.addCode(`chollima.get_class_value()`);

    }
    //% block="是否存在ID[ID]" blockType="boolean"
    //% ID.shadow="number" ID.defl="0"
    export function ai_get_learn_data(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`chollima.get_id_data(${id})`);
    }

    //% block="获取方框[INDEX]参数" blockType="reporter"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX"
    export function ai_get_coord(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        Generator.addCode(`chollima.get_coord_result(${index})`);
    }
    //% block="千里马获取物体分类名称" blockType="reporter"
    export function get_object_name(parameter: any, block: any) {
        Generator.addCode(`chollima.get_object_name()`);
    }
    //% block="---"
    export function noteSep2() {

    }

    //% block="LCD显示文本X[X]Y[Y]内容[MESS]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% MESS.shadow="string" MESS.defl="HELLO WORLD!"
    export function ai_lcd_dispchar(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let mess = parameter.MESS.code;
        Generator.addCode(`chollima.lcd_display_str(${x},${y}, ${mess})`);
    }

    //% block="LCD绘制矩形X[X]Y[Y]长[W]宽[H]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% W.shadow="number" W.defl="0"
    //% H.shadow="number" H.defl="0"    
    export function ai_draw_rectangle(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let w = parameter.W.code;
        let h = parameter.H.code;
        Generator.addCode(`chollima.draw_rectangle([${x},${y},${w},${h}])`);
    } 

    //% block="LCD绘制圆形圆心坐标X[X]Y[Y]半径[R]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% R.shadow="number" R.defl="0"   
    export function ai_draw_circle(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let r = parameter.R.code;
        Generator.addCode(`chollima.draw_circle([${x},${y},${r}])`);
    } 

    //% block="LCD绘制十字X[X]Y[Y]延伸[H]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% H.shadow="number" H.defl="0"    
    export function ai_draw_cross(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let h = parameter.H.code;
        Generator.addCode(`chollima.draw_cross([${x},${y},${h}])`);
    } 

    //% block="LCD绘制三角形顶点X[X]Y[Y]底[W]高[H]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% W.shadow="number" W.defl="0"
    //% H.shadow="number" H.defl="0"    
    export function ai_draw_triangle(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let w = parameter.W.code;
        let h = parameter.H.code;
        Generator.addCode(`chollima.draw_triangle([${x},${y}, ${w},${h}])`);
    } 

    //% block="千里马LCD清除图形" blockType="command"
    export function ai_lcd_clear_graph(parameter: any, block: any) {
        Generator.addCode(`chollima.lcd_graph_clear()`);
    }    

    //% block="千里马LCD清除字符" blockType="command"
    export function ai_lcd_clear_str(parameter: any, block: any) {
        Generator.addCode(`chollima.lcd_display_clear()`);
    }

    //% block="---"
    export function noteSep3() {

    }
    //% block="语音识别 添加关键词[KEY_WORD] 编号为[ASR_NUM]" blockType="command"
    //% KEY_WORD.shadow="string" KEY_WORD.defl="guan-deng"
    //% ASR_NUM.shadow="number" ASR_NUM.defl="0"
    export function ai_asr_addCommand(parameter: any, block: any) {
        let key_word = parameter.KEY_WORD.code;
        let asr_num = parameter.ASR_NUM.code;
        Generator.addCode(`chollima.addCommand(${key_word},${asr_num})`);
    }

    //% block="语音识别 设置识别阈值[ASR_TRL]开始识别" blockType="command"
    //% ASR_TRL.shadow="number" ASR_TRL.defl="0"
    export function ai_asr_start(parameter: any, block: any) {
        let asr_trl = parameter.ASR_TRL.code;
        Generator.addCode(`chollima.asr_start(${asr_trl})`);
    }
    //% block="---"
    export function noteSep5() {

    }

    //% block="运行分类器" blockType="command"
    export function ai_knn_model_train(parameter: any, block: any) {
        Generator.addCode(`chollima.knn_model_train()`);
    }

    //% block="添加到分类[ID]" blockType="command"
    //% ID.shadow="number" ID.defl="0"
    export function ai_add_sample_img(parameter: any, block: any) {
        let id = parameter.ID.code;
        Generator.addCode(`chollima.add_sample_img(${id})`);
    }

    //% block="初始化分类器分类为[CLASS_N]" blockType="command"
    //% CLASS_N.shadow="string" CLASS_N.defl=" "
    export function ai_load_knn_model(parameter: any, block: any) {
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`chollima.load_knn_model(${class_n})`);
    }

    //% block="储存分类器为[MODEL_N]" blockType="command"
    //% MODEL_N.shadow="string" MODEL_N.defl=" "
    export function ai_save_knn_file(parameter: any, block: any) {
        let model_n = parameter.MODEL_N.code;
        Generator.addCode(`chollima.save_knn_file(${model_n})`);
    }

    //% block="加载分类器[MODEL_N]分类器为[CLASS_N]" blockType="command"
    //% MODEL_N.shadow="string" MODEL_N.defl=" "
    //% CLASS_N.shadow="string" CLASS_N.defl=" "
    export function ai_load_knn_file(parameter: any, block: any) {
        let model_n = parameter.MODEL_N.code;
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`chollima.load_knn_file(${model_n}, ${class_n})`);
    }

    //% block="---"
    export function noteSep4() {

    }
    //% block="拍照存储到TF卡[PICTURE]" blockType="command"
    //% PICTURE.shadow="dropdown" PICTURE.options="PICTURE"
    export function ai_photograph(parameter: any, block: any) {
        let picture = parameter.PICTURE.code;
        Generator.addCode(`chollima.picture_capture(${picture})`);
    }

    //% block="录像[TIME]秒存储到TF卡" blockType="command"
    //% TIME.shadow="number" TIME.defl="0"
    export function ai_video(parameter: any, block: any) {
        let time = parameter.TIME.code;
        Generator.addCode(`chollima.video_capture(${time})`);
    }

    //% block="摄像头亮度增益[GAIN]" blockType="command"
    //% GAIN.shadow="number" GAIN.defl="10"
    export function ai_gain(parameter: any, block: any) {
        let gain = parameter.GAIN.code;
        Generator.addCode(`chollima.camera_set_gain(${gain})`);
    }

    //% block="设置特定区域颜色识别X[X] Y[Y] W[W] H[H]" blockType="command"
    //% X.shadow="number" X.defl="0"
    //% Y.shadow="number" Y.defl="0"
    //% W.shadow="number" W.defl="0"
    //% H.shadow="number" H.defl="0"
    export function color_recognize_roi(parameter: any, block: any) {
        let x = parameter.X.code;
        let y = parameter.Y.code;
        let w = parameter.W.code;
        let h = parameter.H.code;
        Generator.addCode(`chollima.color_recognize_roi([${x},${y},${w},${h}])`);
    }

    //% block="从SD卡加载[AI_M]模型[MODEL_N]分类为[CLASS_N]" blockType="command"
    //% AI_M.shadow="dropdown" AI_M.options="AI_M"
    //% MODEL_N.shadow="string" MODEL_N.defl="gesture.kmodel"
    //% CLASS_N.shadow="string" CLASS_N.defl="a,b,c"
    export function ai_sd_model(parameter: any, block: any) {
        let ai_m = parameter.AI_M.code;
        let model_n = parameter.MODEL_N.code;
        let class_n = parameter.CLASS_N.code;
        Generator.addCode(`chollima.loading_sd_model(${ai_m}, ${model_n}, ${class_n})`);
    }

    //% block="读取SD卡模型分类数据" blockType="reporter""
    export function ai_get_model_data(parameter: any, block: any) {
        Generator.addCode(`chollima.get_model_data()`);
    }
}
    

